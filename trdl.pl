#!/usr/bin/perl

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# tr;dl - Too Rong; Didn't Lead - 1.2.0 (2012-04-05)
#
#
# trdl is a torrent analyzer and filterer. It relies heavily on configuration
# files to be as extensible as possible.
#
#
# Copyright (C) 2012 Martin Haggstrom <martin.haggstrom@gmx.com>
#
# Released under the Creative Commons Attribution-ShareAlike License
# (http://creativecommons.org/licenses/by-sa/3.0/)
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

use strict;
use warnings;

use Getopt::Long;
use File::HomeDir;
use YAML::XS;
use Hash::Merge qw( merge );

use trdl::trdl;

my $configfiles = ['~/.trdl/conf.d'];

my $quiet = 0;

GetOptions(
    "c|config=s@"   => \$configfiles,
    "q|quiet!"      => \$quiet,
);

my $conf = {};

sub readFile {
    my $file = shift;
    $conf = merge ($conf, YAML::XS::LoadFile( $file ));
}

foreach my $cfile (@$configfiles)
{
    my $home = File::HomeDir::home();
    $cfile =~ s/^~\//$home\//;

#     print "conf: $cfile\n";

    if (-d $cfile)
    {
        opendir(DIR, $cfile) or die $!;

        while (my $file = readdir(DIR)) {

            next if ($file =~ m/^\./);
            next unless ($file =~ m/\.yml$/);
            $file = "$cfile/$file";
            next unless (-e $file);

#             print "$file\n";
            readFile($file);
        }
        closedir(DIR);
    }
    elsif (-e $cfile)
    {
#         print "$cfile\n";
        readFile($cfile);
    }
    else
    {
        print "$cfile: $!\n";
    }
}

my $trdl = trdl::trdl->new($conf, $quiet);

$trdl->startSources(\@ARGV);

$trdl->processInput();
