package trdl::handler;

use strict;
use warnings;

use trdl::utils;
use trdl::plugins::setter;

use YAML::XS;
use Module::Pluggable instantiate => 'new', search_path => ['trdl::plugins'];

sub new {
    my $class = shift;
    my $conf = shift;
    my $quiet = shift;
 
    my $self = bless({}, $class);

    $self->{'actions'} = $conf->{actions};

    $self->{'globals'} = $conf->{globals};
 
    my @plugins = $self->plugins($conf->{modulesettings});

#     print "plugins: " . @plugins ."\n";
    foreach my $plugin (@plugins)
    {
        my $pluginname = ref($plugin);
        $pluginname =~ s/trdl::plugins:://;
        $self->{'modules'}->{$pluginname} = $plugin;
#         print "plugin: $plugin, " . $pluginname . "\n";
    }
 
    return $self;
}


sub handletorrent {
    my $self = shift;
    my $thash = shift;

    $thash->{'actions'} = ['initializer'] unless $thash->{'actions'};

    while (@{trdl::utils->get_array($thash->{'actions'})})
    {
        $thash->{'actions'} = trdl::utils->get_array($thash->{'actions'});

        my $action = shift($thash->{'actions'});

        next unless ($action);

        if ($self->{'actions'}->{$action})
        {
            my $module = $self->{'actions'}->{$action}->{'module'};

            if ($module and $self->{'modules'}->{$module})
            {
                my ($config, $passed);
                my $precond = $self->{'actions'}->{$action}->{'precond'};
                if ($precond)
                {
                    $passed = $self->{'modules'}->{'setter'}->run($thash, $precond, $action.' (precond)', $self->{'globals'} );
                    unless ($passed)
                    {
                        trdl::utils->appendTo($thash, 'warning',
                            "[$action] precond failed.")
                            unless (defined($precond->{'options'}->{'nowarning'}));
                        next;
                    }
                }
                if ($self->{'actions'}->{$action}->{'config'})
                {
                    $config = $self->{'actions'}->{$action}->{'config'}
                }
                elsif ($self->{'actions'}->{$action}->{'config_file'})
                {
                    $config = trdl::utils->loadfile( $self->{'actions'}->{$action}->{'config_file'} );
                }
                $passed = $self->{'modules'}->{$module}->run($thash, $config, $action, $self->{'globals'});
            
#     print YAML::XS::Dump($thash);

#                 print "$action failed\n" unless ($passed);

                my $postcond = $self->{'actions'}->{$action}->{'postcond'};
                if ($postcond)
                {
                    $passed = $self->{'modules'}->{'setter'}->run($thash, $postcond, $action.' (postcond)', $self->{'globals'});
                    unless ($passed)
                    {
                        trdl::utils->appendTo($thash, 'warning',
                            "[$action] Error: postcond failed.")
                            unless (defined($postcond->{'options'}->{'nowarning'}));
                        last;
                    }
                }
            } else {
                print "Error: No matching module '$module'\n";
            }
        } else {
            print "Error: No matching action '$action'\n";
        }
    }

    delete $thash->{'actions'};
}


1;
