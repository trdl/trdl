
use warnings;
use strict;

use JSON::XS;

my $trdlircplugin;

{
    package trdlBot;
    use base qw(Bot::BasicBot);
    use IRC::Utils ':ALL';

    sub said {
        my $self = shift;
        my $message = shift;

        $message->{body} = strip_color(strip_formatting($message->{body}));

        $trdlircplugin->handlemsg($message);

        return;
    }

    sub kicked {
        my $self = shift;
        my $message = shift;
        my $ts = scalar localtime;

        if ($message->{kicked} eq $self->nick())
        {
            print STDERR "[$ts] $message->{who} KICKED us from $message->{channel}: \"$message->{reason}\"\n";
            $self->shutdown($self->quit_message());
        }

        return;
    }

    sub name {
        my $self = shift;
        $self->{name} = shift if @_;
#         $_[0]->{name} or $self->nick . " bot";
#         print "name: ", $self->{name}, "\n";
        return $self->{name};
    }

    sub help {
        return;
    }
}

package trdl::sources::irc;

use threads;
use threads::shared;
use Thread::Queue;

use trdl::plugins::setter;

sub handlemsg {
    my $self = shift;
    my $message = shift;

    my $ts = scalar localtime;
#     print "[$ts] <$message->{who}\@$message->{channel}> $message->{body}\n";

    my $hash = {};
    while (my ($key, $value) = each %{$message})
    {
        $hash->{'irc-'.$key} = $value;
    }
    $hash->{'source'} = $self->{'source'};

    my $passed = $self->{'setter'}->run($hash, $self->{'config'}, $self->{'source'});

    while (my ($key, $value) = each %{$hash})
    {
        delete $hash->{$key} if ($key =~ m/^irc-/);
    }

#     print "[$ts] <$message->{who}\@$message->{channel}> $message->{body}\n";
    if ($passed)
    {
        $self->{queue}->enqueue(JSON::XS::encode_json($hash));
    }

    return $self;
}

sub new {
    my $class = shift;
    my $config = shift;
    my $source = shift;
 
    my $self = bless({}, $class);

    $self->{'ircsettings'} = $config->{'ircsettings'};
    $self->{'source'} = $source;
    $self->{'config'} = $config;
 

    print STDERR "  '".$self->{'source'}."' starting.\n";

    return $self;
}

sub run {
    my $self = shift;
    my $queue = shift;

    $self->{'queue'} = $queue;

    $self->{'setter'} = trdl::plugins::setter->new;

    $trdlircplugin = $self;

    trdlBot->new(%{$self->{'ircsettings'}})->run();

    return 1;
}

1;
