package trdl::sources::rss;

use strict;
use warnings;

use Thread::Queue;
use LWP::Simple;

use trdl::plugins::setter;

use XML::RSS::LibXML;
use JSON::XS;

sub new {
    my $class = shift;
    my $config = shift;
    my $source = shift;

    my $self = bless({}, $class);

    unless (defined($config->{'url'}))
    {
        die "[$class] Error: not enough settings available.";
    }

    $self->{'interval'} = defined($config->{'interval'}) ? $config->{'interval'} : 30;
    $self->{'interval'} = 30 if ($self->{'interval'} < 3);
    $self->{'source'} = $source;
    $self->{'url'} = $config->{'url'};
    $self->{'config'} = $config;

    print STDERR "  '".$self->{'source'}."' polling '".$self->{'url'}."' each ".$self->{'interval'}." minutes.\n";

    return $self;
}

sub run {
    my $self = shift;
    my $queue = shift;

    my $rss = XML::RSS::LibXML->new;
    my $setter = trdl::plugins::setter->new;

    my $oldentry;
    my $firstrun = 1;

    do {
        my $feed = get $self->{'url'};

#         print "got feed: $feed \n";
#         print "updating feed\n";
        my $guid = 'guid';

        $rss->parse($feed);

        $oldentry = $rss->{items}->[0]->{$guid}
            if ($firstrun and @{$rss->{items}} and $self->{'config'}->{'skipexisting'});
        $firstrun = 0;

        foreach my $item (@{ $rss->{items} }) {
#         print "item: $item->{'link'}\n";
            last if (defined($oldentry) and $oldentry eq $item->{$guid});
#             print "  item: $item->{title} ($item->{link})\n";
            my $hash = {};
            while (my ($key, $value) = each %{$item})
            {
                $hash->{'rss-'.$key} = $value;
            }
            $hash->{'source'} = $self->{'source'};

            $setter->run($hash, $self->{'config'}, $self->{'source'});

            while (my ($key, $value) = each %{$hash})
            {
                delete $hash->{$key} if ($key =~ m/^rss-/);
            }

            $queue->enqueue(JSON::XS::encode_json($hash));
        }
        $oldentry = $rss->{items}->[0]->{$guid} if (@{$rss->{items}});

#         $queue->enqueue($msg);

    } while (sleep($self->{'interval'} * 60));

    $queue->enqueue($self->{'source'} . " terminating");

    return 1;
}

1;
