package trdl::sources::command_line;

use strict;
use warnings;

use threads;
use threads::shared;
use Thread::Queue;

use trdl::utils;

use JSON::XS;

sub new {
    my $class = shift;
    my $config = shift;
    my $source = shift;
 
    my $self = bless({}, $class);

    my $format = defined($config->{'format'}) ? $config->{'format'} : 'json';

    my $options = $config->{'options'};

    $self->{'source'} = $source;
    $self->{'options'} = $options;
    $self->{'format'} = $format;

    die "[$class] Error: not enough settings available."
        unless (trdl::utils::checkAvailableOptions($format, $options));

    print STDERR "  '".$self->{'source'}."' parsing command line in format '".$self->{'format'}."'.\n";

    return $self;
}

sub run {
    my $self = shift;
    my $queue = shift;
    my $input = shift;

    foreach my $torrent (@$input) {
        if ($torrent)
        {
            my $hash = trdl::utils::parseEntry($self->{'format'}, $self->{'options'}, $torrent);
            if (ref($hash) eq 'HASH')
            {
                $hash->{'source'} = $self->{'source'};

                $queue->enqueue(JSON::XS::encode_json($hash));
            }
        }
    }

    $queue->enqueue($self->{'source'} . " terminating");

    return 1;
}


1;
