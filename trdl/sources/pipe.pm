package trdl::sources::pipe;

use strict;
use warnings;

use threads;
use threads::shared;
use Thread::Queue;

use trdl::utils;

use JSON::XS;

my $checkoptions = {
    'csv'   => sub {
        my $options = shift;
        return  (defined($options->{'fields'}) and
            ref($options->{'fields'}) eq 'ARRAY' and
            defined($options->{'delimiter'}));
    },
    'json'   => sub {
        return  1;
    },
};

sub new {
    my $class = shift;
    my $config = shift;
    my $source = shift;

    my $self = bless({}, $class);

    my $f = "*STDIN";

    my $format = defined($config->{'format'}) ? $config->{'format'} : 'json';

    my $options = $config->{'options'};

    my $filehandle;
    if (defined $config->{'pipe'}) {
        open ($filehandle, "+< $config->{'pipe'}") or die $!;
        $f = $config->{'pipe'};
    } else {
        $filehandle = *STDIN;
    }
    $self->{'source'} = $source;
    $self->{'pipe'} = $filehandle;
    $self->{'options'} = $options;
    $self->{'format'} = $format;

    die "[$class] Error: not enough settings available."
        unless ($checkoptions->{$format}->($options));

    print STDERR "  '".$self->{'source'}."' listening on '".$f."' in format '".$self->{'format'}."'.\n";

    return $self;
}

sub run {
    my $self = shift;
    my $queue = shift;

    my $pipe = $self->{'pipe'};

    while (<$pipe>) {
        chomp;
        if (s/\\$//) {
            $_ .= <$pipe>;
            redo unless eof();
        }
        if ($_)
        {
            my $hash = trdl::utils::parseEntry($self->{'format'}, $self->{'options'}, $_);

            if (ref($hash) eq 'HASH')
            {
                $hash->{'source'} = $self->{'source'};

                $queue->enqueue(JSON::XS::encode_json($hash));
            }
        }
    }
    $queue->enqueue($self->{'source'} . " terminating");

    return 1;
}


1;
