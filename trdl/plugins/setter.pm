package trdl::plugins::setter;

use strict;
use warnings;

use trdl::utils;

use List::Compare;
use List::Util qw(max min);

my $filter_functions = {
    "all"       => \&filter_all,
    "any"       => \&filter_any,
    "not"       => \&filter_not,
    "max"       => \&filter_max,
    "min"       => \&filter_min,
    "regex"     => \&filter_regex_any,
    "regex-any" => \&filter_regex_any,
    "regex-not" => \&filter_regex_not,
};

sub genValue {
    my $self = shift;
    my $thash = shift;
    my $value = shift;
    my $regexmatches = shift;
    my $globals = shift;

    my $newvalues = trdl::utils->get_array($value);
    my $newstuffs;
    foreach my $newvalue (@$newvalues)
    {
        my $newstuff = trdl::utils->generateField($thash, $newvalue, $globals, $regexmatches);
# print "newstuff:", YAML::XS::Dump($newstuff);
        $newstuffs = trdl::utils->merge_arrays($newstuffs, $newstuff) if ($newstuff);
    }
# print "newstuffs:", YAML::XS::Dump($newstuffs);
    return $newstuffs;
}

sub new {
    my $class = shift;

    my $self = bless({}, $class);

    return $self;
}

sub run {
    my $self = shift;
    my $thash = shift;
    my $config = shift;
    my $action = shift;
    my $globals = shift;

    my $anypassed = 0;

    my $filters = $config->{'values'};

    my $options = $config->{'options'};

    my $run_all = 0;

    $run_all = 1 if ($options and $options->{'run-all'});

    $filters = [$filters] unless (ref $filters eq 'ARRAY');

    foreach my $filter (@$filters)
    {
        my $passed = 0;
        my $regexmatches = [];
        my $p = 1;
        while ( my ($key, $value) = each %{$filter})
        {
            next if ($key eq 'set' or $key eq 'unset' or $key eq 'inject' or $key eq 'add');
            my %values;
            unless (ref($value)) {
                my @temp = ($value);
                $values{'any'} = \@temp;
            } elsif (ref($value) eq 'ARRAY') {
                $values{'any'} = $value;
            } elsif (ref($value) eq 'HASH') {
                while ( my ($subkey, $subvalue) = each %{$value})
                {
                    unless (ref($subvalue)) {
                        my @temp = ($subvalue);
                        $values{$subkey} = \@temp;
                    } elsif (ref($subvalue) eq 'ARRAY') {
                        $values{$subkey} = $subvalue;
                    }
                }
            } else {
                %values = %{$value};
            }

            my $othervalues;
            if ($key eq 'isset')
            {
                my @garray = keys $globals;

                for my $g (@garray)
                {
                    $g = '__'.$g;
                }

                my @array = (keys $thash, @garray);
                $othervalues = \@array;
            }
            elsif ($key =~ m/^__(.*)$/)
            {
                $othervalues = trdl::utils->get_array($globals->{$1});
            }
            else
            {
                $othervalues = trdl::utils->get_array($thash->{$key});
            }

            while ( my ($filtering, $subvalue) = each %values)
            {
                my $newstuffs = $self->genValue($thash, $subvalue, $regexmatches, $globals);

                $newstuffs = trdl::utils->get_array($newstuffs);

#                 print "OTHER:", YAML::XS::Dump($othervalues);
#                 print "SOME:", YAML::XS::Dump($newstuffs);

                $p = 0 unless $filter_functions->{$filtering}->($newstuffs,
                  $othervalues, $regexmatches);
            }


        }
        $passed = 1 if ($p == 1);
        if ($passed)
        {
            $anypassed = 1;

            while ( my ($key, $value) = each %{$filter->{'set'}})
            {
                my $newstuffs = $self->genValue($thash, $value, $regexmatches, $globals);
                my $genkey = trdl::utils->generateField($thash, $key, $globals, $regexmatches);
                if ($genkey =~ m/^__(.*)$/) {
                    $globals->{$1} = $newstuffs if defined($newstuffs);
                } else {
                    $thash->{$genkey} = $newstuffs if defined($newstuffs);
                }
            }
            while ( my ($key, $value) = each %{$filter->{'add'}})
            {
                my $newstuffs = $self->genValue($thash, $value, $regexmatches, $globals);
                my $genkey = trdl::utils->generateField($thash, $key, $globals, $regexmatches);
#                 my $tmpp = trdl::utils->merge_arrays($thash->{$key}, $newstuffs) if defined($newstuffs);
                if ($genkey =~ m/^__(.*)$/) {
                    my $tmpp = trdl::utils->merge_arrays($globals->{$1}, $newstuffs) if defined($newstuffs);
                    $globals->{$1} = $tmpp if defined($newstuffs);
                } else {
                    my $tmpp = trdl::utils->merge_arrays($thash->{$genkey}, $newstuffs) if defined($newstuffs);
                    $thash->{$genkey} = $tmpp if defined($newstuffs);
                }
            }
            while ( my ($key, $value) = each %{$filter->{'inject'}})
            {
                my $newstuffs = $self->genValue($thash, $value, $regexmatches, $globals);
                my $genkey = trdl::utils->generateField($thash, $key, $globals, $regexmatches);
#                 my $tmpp = trdl::utils->merge_arrays($newstuffs, $thash->{$key}) if defined($newstuffs);
                if ($genkey =~ m/^__(.*)$/) {
                    my $tmpp = trdl::utils->merge_arrays($newstuffs, $globals->{$1}) if defined($newstuffs);
                    $globals->{$1} = $tmpp if defined($newstuffs);
                } else {
                    my $tmpp = trdl::utils->merge_arrays($newstuffs, $thash->{$genkey}) if defined($newstuffs);
                    $thash->{$genkey} = $tmpp if defined($newstuffs);
                }
            }
            foreach my $key (@{trdl::utils->get_array($filter->{'unset'})})
            {
                my $genkey = trdl::utils->generateField($thash, $key, $globals, $regexmatches);
                if ($genkey =~ m/^__(.*)$/) {
                    delete $globals->{$1};
                } else {
                    delete $thash->{$genkey};
                }
            }

            last unless $run_all;
        }
    }

    return $anypassed;
}

sub filter_any {
    my $values = shift;
    my $othervalues = shift;

    my $lc = List::Compare->new('-u', $values, $othervalues);

    return $lc->get_intersection;
}

sub filter_all {
    my $values = shift;
    my $othervalues = shift;

    my $lc = List::Compare->new('-u', $values, $othervalues);

    return $lc->is_LsubsetR;
}

sub filter_not {
    my $values = shift;
    my $othervalues = shift;

    my $lc = List::Compare->new('-u', $values, $othervalues);

    return not $lc->get_intersection;
}

sub filter_max {
    my $values = shift;
    my $othervalues = shift;

    no warnings 'uninitialized';

    return (min(@$values) >= max(@$othervalues));
}

sub filter_min {
    my $values = shift;
    my $othervalues = shift;

    no warnings 'uninitialized';

    return (max (@$values) <= min (@$othervalues));
}

sub filter_regex_any {
    my $regexes = shift;
    my $values = shift;
    my $regexmatches = shift;

    foreach my $regex (@$regexes)
    {
        foreach my $value (@$values)
        {
#             print STDERR "<$value> matches <$regex> ?\n";
            if (my @matches = $value =~ m/$regex/i)
            {
                foreach my $match (@matches)
                {
                    push(@$regexmatches, $match);
                }
                return 1;
            }
        }
    }

    return 0;
}

sub filter_regex_not {
    my $regexes = shift;
    my $values = shift;
    my $regexmatches = shift;

    my $match = 0;
    foreach my $regex (@$regexes)
    {
        foreach my $value (@$values)
        {
            if ($value =~ m/$regex/i)
            {
                $match = 1;
            }
        }
    }

    return not $match;
}

1;
