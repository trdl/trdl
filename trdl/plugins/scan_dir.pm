package trdl::plugins::scan_dir;

use strict;
use warnings;

use trdl::utils;
use trdl::handler;

use File::Find;
use YAML::XS;

sub new {
    my $class = shift;
    my $config = shift;
 
    my $self = bless({}, $class);
 
    return $self;
}

sub run {
    my $self = shift;
    my $thash = shift;
    my $config = shift;
    my $action = shift;

    my @dirs = ('/home/mhn/athena/data/torrents/');

    my @ignore = (
        '\.r\d\d$',
        '^sample$',
        '\.nfo$',
        '\.sfv$',
        '\.rar$',
        '\.torrent$',
        '^S\d\d$',
        '^TV$',
        '^Movies$',
        '^Games$',
        '^Anime$',
        '^\.',
        '\Wsample\.\w{3}$',
    );
        
    my %labelmap = (
        '^TV'       => 'TV',
        '^Movies'   => 'Movies',
        '^Games'    => 'Games',
        '^Anime'    => 'Anime'
    );

    my $wanted = sub {
        my $filename = $_;

        my $passed = 1;
        foreach my $i (@ignore)
        {
            if ($filename =~ m/$i/i)
            {
                $passed = 0;
                last;
            }
        }

        $passed or return;

        my $dirname = $File::Find::dir;

        my $path = '/home/mhn/athena/data/torrents/';
        my $label = '';

        $dirname =~ s/^\Q$path\E//;

        while ( my ($key, $value) = each %labelmap)
        {
            if ($dirname =~ m/$key/)
            {
                $label = $value;
                last;
            }
        }

        $filename =~ s/\.[a-z]{3}$//;
        $filename =~ s/\[[A-F0-9]{8}\]//;
        $filename =~ s/\./ /g;
        $filename =~ s/(^\s|\s$)//g;

        my $mhash = {};
        $mhash->{'label'} = $label;
        $mhash->{'torrent'} = $filename;
        $mhash->{'actions'} = ['analyzer', 'analyze_unknown_tags'];

    #     print YAML::XS::Dump($mhash);

        my $handler = trdl::handler->new('/home/mhn/Development/git/trdl/actions.yml', 0);

        $handler->handletorrent($mhash);

        print YAML::XS::Dump($mhash) if (defined($mhash->{'analyzer'}));

    };

    finddepth(\&$wanted, @dirs);

    return 1;
}

# run();

1;
