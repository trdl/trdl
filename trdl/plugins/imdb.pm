package trdl::plugins::imdb;

use strict;
use warnings;

use trdl::utils;
use LWP::Simple;
use URI::Escape;
use File::Path qw(mkpath);

sub new {
    my $class = shift;
    my $config = shift;
 
    my $self = bless({}, $class);
 
    return $self;
}

sub run {
    my $self = shift;
    my $thash = shift;
    my $config = shift;
    my $action = shift;


    unless (defined($thash->{'title'}))
    {
        return 0;
    }

    my $url = 'http://www.imdbapi.com/?';
    $url .= 'r=json&';
    $url .= 'tomatoes=true&'
        if (defined($config->{'tomatoes'}) and $config->{'tomatoes'});
    $url .= 't='.uri_escape($thash->{'title'}).'&'
        if defined($thash->{'title'});
    $url .= 'y='.uri_escape($thash->{'year'}).'&'
        if (defined($thash->{'year'}) and $thash->{'year'} =~ m/^\d{4}$/);

#     print "my URL = $url\n";

    my $response = get $url;

    unless ($response)
    {
        trdl::utils->appendTo($thash, 'warning',
            "[$action] No response from imdbapi.");
        return 0;
    }

    my $imdbinfo = JSON::Syck::Load( $response );

    print "DUMP:", YAML::XS::Dump($imdbinfo), "\n";

    return 0 unless defined($imdbinfo->{'Title'});

    my $imdbtitle = $imdbinfo->{'Title'};
    if (defined($imdbinfo->{'Title'}) and $thash->{'title'} =~ m/^$imdbtitle$/i)
    {
        print "got the right movie\n";
        while ( my ($key, $value) = each %{$imdbinfo})
        {
            my $field = $key =~ m/^tomato/ ? $key : 'imdb'.$key;
            $thash->{$field} = $value unless ($value eq 'N/A');
        }
    }

    return 1;
}

1;
