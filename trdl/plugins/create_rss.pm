package trdl::plugins::create_rss;

use strict;
use warnings;

use trdl::utils;

use XML::RSS::LibXML;
use DateTime;

sub new {
    my $class = shift;
    my $config = shift;

    my $self = bless({}, $class);

    return $self;
}

sub run {
    my $self = shift;
    my $thash = shift;
    my $config = shift;
    my $action = shift;

    unless (defined($config->{'file'}))
    {
        trdl::utils->appendTo($thash, 'error',
            "[$action] Not enough settings available (config field 'file' required).");
        return 0;
    }

    my $feedlocation = $config->{'file'};

    my $rss = XML::RSS::LibXML->new(version => '2.0');

    if (-e $feedlocation) {
        $rss->parsefile($feedlocation);
    }
    else
    {
        $rss->channel(
            title       => $config->{'feedtitle'},
            link        => $config->{'feedurl'},
            description => $config->{'feeddescription'},
        );
    }

#     my $current_time = localtime();
    my $current_time = DateTime->now(formatter => DateTime::Format::Mail->new());

    my %rssitem;

    $rssitem{title} = trdl::utils->generateField($thash, $config->{'itemtitle'})
        if (defined($config->{'itemtitle'}));
    $rssitem{link} = trdl::utils->generateField($thash, $config->{'itemurl'})
        if (defined($config->{'itemurl'}));
    $rssitem{description} = trdl::utils->generateField($thash, $config->{'itemdescription'})
        if (defined($config->{'itemdescription'}));
    $rssitem{guid} = $current_time.'-'.$rssitem{title};
    $rssitem{pubDate} = $current_time;
    $rssitem{mode} = 'insert';

    $rss->add_item(%rssitem);

    while (@{$rss->{'items'}} > $config->{'itemlimit'})
    {
        pop(@{$rss->{'items'}});
    }

    $rss->save($feedlocation);


    return 1;
}

1;
