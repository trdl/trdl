package trdl::plugins::download_rutorrent;

use strict;
use warnings;

use trdl::utils;
use LWP::Simple;
use URI::Escape;
use File::Path qw(mkpath);

sub new {
    my $class = shift;
    my $config = shift;
 
    my $self = bless({}, $class);
 
    return $self;
}

sub run {
    my $self = shift;
    my $thash = shift;
    my $config = shift;
    my $action = shift;

    unless (defined($config->{'rutorrent-url'}) and defined($config->{'basepath'}))
    {
        trdl::utils->appendTo($thash, 'error',
            "[$action] Not enough settings available.");
        return 0;
    }

    my $path = $config->{'basepath'} . '/' . $thash->{'path'};
    my $label = $thash->{$config->{'label-field'}};
    my $stopped = $config->{'stopped'};
    $stopped = $thash->{'stopped'} if defined($thash->{'stopped'});

# print "$path\n";

    mkpath ($path);

    my $url = $config->{'rutorrent-url'} . '/php/addtorrent.php'.
              '?url=' . uri_escape($thash->{'url'}) .
              '&label=' . uri_escape($thash->{'longlabel'}) .
              '&dir_edit=' . uri_escape($path);
    $url .= '&torrents_start_stopped=' . uri_escape($stopped) if ($stopped);

    my $response = get $url;

    unless ($response eq "log(theUILang.addTorrentSuccess);")
    {
        trdl::utils->appendTo($thash, 'error',
            "[$action] Unknown response from server: ".$response);
        return 0;
    }

    return 1;
}

1;
