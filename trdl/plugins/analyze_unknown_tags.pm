package trdl::plugins::analyze_unknown_tags;

use strict;
use warnings;

use trdl::utils;

sub new {
    my $class = shift;
    my $config = shift;

    my $self = bless({}, $class);

    return $self;
}

sub run {
    my $self = shift;
    my $thash = shift;
    my $config = shift;
    my $action = shift;

    $self->{'fields'} = {};

    $self->{'tags'} = $config;

    while ( my ($tkey, $tvalue) = each %{$self->{'tags'}})
    {
        $self->{'fields'}->{$tkey} = [];
    }

    while ( my ($key, $value) = each %{$thash})
    {
        if ($key =~ m/((.*)_)?unknown(_(.*))?/)
        {
            my $method = defined($2)? 'pop' : defined($4)? 'shift' : 'pop';
            my $restkey = defined($2)? $2 : defined($4) ? $4 : 'junk';

#             print "found a tag containg 'unknown': <$value> ($restkey, $method)\n";

            my ($match, $nmatch) = (0, 0);
            do {
                $match = 0;
#                 print "entering loop\n";
                while ( my ($tkey, $tvalue) = each %{$self->{'tags'}})
                {
                    ($value, $nmatch) = $self->compare($value, $tkey, $self->{'tags'}->{$tkey});
#                     print "this one match? $nmatch, any matched? $match\n";
                    $match = ($match or $nmatch);
                }
#                 ($value, $omatch) = $self->compare($value, 'other');
            } while ($match);

#             print "q:", @{$self->{'quality'}}, " o:", @{$self->{'other'}}, " < $value\n";

            $value =~ s/(^[\W_]*)|([\W_]*$)//g;

            $thash->{$restkey} = trdl::utils->merge_arrays($thash->{$restkey}, $value)
                if ($value);
            delete $thash->{$key};
        }
    }

    while ( my ($tkey, $tvalue) = each %{$self->{'tags'}})
    {
        $thash->{$tkey} = trdl::utils->merge_arrays($thash->{$tkey}, $self->{'fields'}->{$tkey})
            if (@{$self->{'fields'}->{$tkey}});
    }

    return 1;
}

sub compare {
    my $self = shift;
    my $value = shift;
    my $key = shift;
    my $tags = shift;

#     print "going to compare against $key\n";

    my $anymatch = 0;
    my $match;
    do {
        $match = 0;
        foreach my $tag (@{$tags})
        {
#             print "comparing <$value> to: <$tag>\n";
            next if ('' =~ m/(^|\W)($tag)\W*$/i);

            if ($value =~ s/(^|\W)($tag)\W*$/ /i)
            {
#                 print "$tag matches: $&\n";
                my $v = $2;
                $v =~ s/(^[\W_]*)|([\W_]*$)//g;
                push(@{$self->{'fields'}->{$key}}, $v);
                $match = 1;
                $anymatch = 1;
            }
        }
    } while ($match);

    return ($value, $anymatch);
}

1;
