package trdl::plugins::exec_blk;

use strict;
use warnings;

use Proc::Background;
use Cwd;
use File::Path qw(mkpath);

use trdl::utils;

sub new {
    my $class = shift;
    my $config = shift;
 
    my $self = bless({}, $class);
 
    return $self;
}

sub run {
    my $self = shift;
    my $thash = shift;
    my $config = shift;
    my $action = shift;
    my $globals = shift;

    my @args = (trdl::utils->generateField($thash, $config->{command}, $globals));

    foreach my $arg (@{$config->{args}})
    {
        push @args, trdl::utils->generateField($thash, $arg, $globals);
    }

    my $p = getcwd;
    my $chdir;

    my $commandstring =  join(',', @args);

# print YAML::XS::Dump($globals);
     print "[exec_blk]: ", $commandstring, "\n";

    $chdir = trdl::utils->generateField($thash, $config->{chdir}, $globals)
        if defined($config->{chdir});

#     print "$chdir\n";
    if (defined($chdir)) {
        mkpath($chdir);
        chdir($chdir);
    }

    my $value = `$commandstring`;

    $value =~ s/(^\s+)|(\s+$)//g;

    chdir($p);

    my $lines;

    @{$lines} = split('\n', $value);

    $thash->{"${action}stdout"} = $lines if @$lines;

    return 1;
}


1;
