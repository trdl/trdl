package trdl::trdl;

use strict;
use warnings;

use threads;
use threads::shared;
use Thread::Queue;

use trdl::handler;

use YAML::XS;
use JSON::XS;

use Module::Pluggable search_path => ['trdl::sources'], require => 1;

sub new {
    my $class = shift;
    my $conf = shift;
#     my $sourcefile = shift;
#     my $actionfile = shift;
    my $quiet = shift;

    my $self = bless({}, $class);

    $self->{'sources'} = $conf->{sources};
    $self->{'globals'} = $conf->{globals};

#     unless (defined($config->{'actions_file'}))
#     {
#         die "[$class] Error: not enough settings available.";
#     }

    $self->{'handler'} = trdl::handler->new($conf, $quiet);

    my @plugins = $self->plugins();

#     print "plugins: " . @plugins ."\n";
    foreach my $plugin (@plugins)
    {
        my $pluginname = $plugin;
        $pluginname =~ s/trdl::sources:://;
        $self->{'modules'}->{$pluginname} = $plugin;
#         print "plugin: $plugin, alias: $pluginname\n";
    }

    return $self;
}


sub startSources {
    my $self = shift;
    my $input = shift;

    $self->{'queue'} = Thread::Queue->new();

    $self->{'threads'} = {};

    while ( my ($source, $soptions) = each %{$self->{'sources'}})
    {
        my $currentmodule = $self->{'modules'}->{$soptions->{'module'}};
        my $func = $currentmodule.'::run';

        print STDERR "Starting ".$soptions->{'module'}." source '".$source."':\n";

        my $instance = $currentmodule->new($soptions->{'config'}, $source);

        my $thread = threads->new($func, $instance, $self->{'queue'}, $input);

        $self->{'threads'}->{$source} = $thread;
    }
}

sub processInput {
    my $self = shift;

    while (my $json = $self->{'queue'}->dequeue()) {
        if ($json =~ m/^(.*) terminating$/)
        {
            $self->{'threads'}->{$1}->join();
            delete $self->{'threads'}->{$1};
            print STDERR "Source '$1' terminated, ".keys(%{$self->{'threads'}})." source(s) still serving.\n";
            last unless (keys(%{$self->{'threads'}}));

        }
        else
        {
            my $thash = JSON::XS::decode_json( $json );

            $self->{'handler'}->handletorrent($thash);

            print YAML::XS::Dump($self->{'globals'});
            print YAML::XS::Dump($thash) unless ($self->{'quiet'});
            print "\n";
        }
    }

}


1;
