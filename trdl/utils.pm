package trdl::utils;

use strict;
use warnings;

use List::Util qw(max);

use JSON::XS;
use YAML::XS;

sub appendTo {
    my $class = shift;
    my $hash = shift;
    my $field = shift;
    my $value = shift;

    my $array = get_array($hash->{$field});

    push(@$array, $value);
    $hash->{$field} = $array;
}

sub merge_arrays {
    my $class = shift;
    my $values = shift;
    my $othervalues = shift;

    $values = trdl::utils->get_array($values);
    $othervalues = trdl::utils->get_array($othervalues);

    for my $v (@$othervalues)
    {
        unless ($v eq '')
        {
            push $values, $v;
        }
    }

    if (@$values eq 1)
    {
        $values = $values->[0];
    }

    return $values;
}

sub get_array {
    my $class = shift;
    my $values = shift;

    $values = [] unless (defined($values));
    $values = [$values] unless (ref($values));

    return $values;
}
#
# sub generateField2 {
#     my $class = shift;
#     my $thash = shift;
#     my $values = shift;
#     my $extrafields = shift;
#
# #     print "EXTRAFIELDS: ".join(", ", @$extrafields)."\n";
#
#     $values = trdl::utils->get_array($values);
#
#     my $failsafe = 0;
#
#     for (my $index = 0; $index < @$values; $index++)
#     {
# #         print "doining nr $index: $values->[$index]\n";
#         while ($index < @$values and $values->[$index] =~ m/\{([^\{\}]*)\{([^\{\}]*)\}([^\{\}]*)\}/)
#         {
# #             print "whiling nr $index: $values->[$index]\n";
#             my ($orig, $pre, $field, $post) = ($&, $1, $2, $3);
#             $failsafe++;
#             if ($failsafe > 30)
#             {
#                 print "infinite(?) loop detected!\n";
#                 print "index: $index, value: $values->[$index]\n";
#                 print YAML::XS::Dump($thash);
#                 print "EXTRAFIELDS: ".join(", ", @$extrafields)."\n";
#                 print "VALUES: ".join(", ", @$values)."\n";
#                 die "inf loop!";
#             }
#
#             my $fieldname = $field;
#             $fieldname =~ s/[:=].*//g;
#
#             my $entry = '';
#             if ($fieldname =~ m/^(.*)\(\)$/)
#             {
# #                 $entry = $builtinfunctions->{$1}->($thash) if defined($builtinfunctions->{$1});
#             }
#             elsif ($fieldname =~ m/^\d+$/)
#             {
#                 $entry = $extrafields->[$fieldname] if defined($extrafields->[$fieldname]);
#             }
#             else
#             {
#                 $entry = $thash->{$fieldname} if defined($thash->{$fieldname});
#             }
#
#             $entry = trdl::utils->get_array($entry);
#
#             $entry->[0] = '' unless (@$entry);
#
# #             print "got entries: ", join (", ", @$entry), "\n";
#
#             my @newvalues;
#             foreach my $e (@$entry)
#             {
#                 my $replacement = '';
#
#                 if ($e =~ m/^(\d+)$/ and $field =~ m/:(\d+)$/)
#                 {
#                     my $pad = $1;
#                     $e = sprintf("%0${pad}d", $e);
#                 }
#                 $replacement = $pre.$e.$post unless ($e eq '');
#                 my $v = $values->[$index];
# #                 print "REGEXING: <$orig> -> <$replacement> in <$v>\n";
#                 $v =~ s/\Q$orig\E/$replacement/;
#                 $v =~ s/(^\s+)|(\s+$)//g;
#                 push (@newvalues, $v) unless ($v eq '');
#             }
#             splice (@$values, $index, 1, @newvalues);
#         }
#     }
#     $failsafe = 0;
# #     print "halfway there\n";
#     for my $index (0 .. $#$values)
#     {
#         while ($values->[$index] =~ m/<(.*)<(.*)><(.*)>(.*)>/) {
#             my ($orig, $pre, $pattern, $subst, $post) = ($&, $1, $2, $3, $4);
#             $failsafe++;
#             if ($failsafe > 30)
#             {
#                 print "infinite(?) loop detected2!\n";
#                 print "index: $index, value: $values->[$index]\n";
#                 print YAML::XS::Dump($thash);
#                 print "EXTRAFIELDS: ".join(", ", @$extrafields)."\n";
#                 print "VALUES: ".join(", ", @$values)."\n";
#                 die "inf loop2!";
#             }
#             $pre =~ s/$pattern/$subst/g;
#             $values->[$index] =~ s/\Q$orig\E/$pre/;
#             $values->[$index] =~ s/(^\s+)|(\s+$)//g;
#         }
#     }
# #     print "allway there\n";
#     return $values;
# }

sub generateField {
    my $class = shift;
    my $thash = shift;
    my $value = shift;
    my $globals = shift;
    my $extrafields = shift;


    my $builtinfunctions = {
        'dumpyaml'  =>
            sub {
                my $thash = shift;
                return YAML::XS::Dump($thash);
            },
        'time'  =>
            sub {
                my $thash = shift;
                return time;
            },
    };

    my $newvalues = $value;
    my $changed;
    do {
        $value = trdl::utils->get_array($newvalues);
        $newvalues = [];

        $changed = 0;
        for my $cvalue (@$value)
        {
            if ($cvalue =~ m/\{([^\{\}]*)\{([^\{\}]*)\}([^\{\}]*)\}/)
            {
                my ($orig, $pre, $afield, $post) = ($&, $1, $2, $3);

                $changed = 1;
                my $field = $afield;
                my $fieldname = $field;
                $fieldname =~ s/[:=\+-].*//g;
                $fieldname =~ s/[#]//g;
                $fieldname =~ s/\[.*//g;
#     print "FIEEEEELD:", $field, "FIEEELDNAME",  $fieldname, "\n";
                my $entry;
                if ($fieldname =~ m/^(.*)\(\)$/)
                {
                    $entry = $builtinfunctions->{$1}->($thash) if defined($builtinfunctions->{$1});
                }
                elsif ($fieldname =~ m/^\d+$/)
                {
                    $entry = $extrafields->[$fieldname] if defined($extrafields->[$fieldname]);
                }
                elsif ($fieldname =~ m/^__(.*)$/)
                {
                    $entry = $globals->{$1} if defined($globals->{$1});
    #print "entry:", YAML::XS::Dump($globals);
                }
                else
                {
                    $entry = $thash->{$fieldname} if defined($thash->{$fieldname});
                }

                if ($field =~ m/^.*\[(\d+)\]$/)
                {

#     print "FIEEEEELD:", $field, "FIEEELDNAME",  $1, "\n";
# YAML::XS::Dump($entry);
                    $entry = $entry->[$1] if (ref($entry) eq 'ARRAY');
                }

                $entry = trdl::utils->get_array($entry);

                if ($field =~ m/^#(.*)$/)
                {
                    $entry = @$entry;
                    $entry = trdl::utils->get_array($entry);
                }


                for my $e (@$entry)
                {
                    if (defined($e) and $field =~ m/-(\d+)/)
                    {
                        $e -= $1;
                    }

                    if (defined($e) and $field =~ m/\+(\d+)/)
                    {
                        $e += $1;
                    }

                    if (defined($e) and $field =~ m/:(\d+)/)
                    {
                        my $pad = $1;
                        $e = sprintf("%0${pad}d", $e);
                    }
                }

                if (not defined($entry) and $field =~ m/=(\w+)/)
                {
                    $entry = trdl::utils->get_array($1);
                }
                for my $e (@$entry)
                {
                    $e = $pre.$e.$post;
                    my $v = $cvalue;
                    $v =~ s/\Q$orig\E/$e/;
                    push(@$newvalues, $v);
                }
#                 print "innerer loop\n";
            }
#             print "inner loop\n";
        }
#         print "outer loop\n";
    } while ($changed eq 1);
#     print "halfway there0\n";

    for my $cvalue (@$value)
    {
        while ($cvalue =~ m/<(.*?)<(.*)><(.*)>(.*)>/) {
            my ($orig, $pre, $pattern, $subst, $post) = ($&, $1, $2, $3, $4);
#             print "SUBST: $orig, $pre, $pattern, $subst, $post\n";
            if ($post eq 'ee') {
                $pre =~ s/$pattern/$subst/gee;
            } else {
                $pre =~ s/$pattern/$subst/g;
            }
            $cvalue =~ s/\Q$orig\E/$pre/;
#             print "SUBST: $orig, $pre, $pattern, $subst, $post => $cvalue\n";
        }
    #     print "allway there0\n";

        $cvalue =~ s/(^\s+)|(\s+$)//g;
    }
    if (@$value eq 1)
    {
        $value = $value->[0];
    }

#     print YAML::XS::Dump($value);

    return $value;
}

sub loadfile {
    my $class = shift;
    my $file = shift;

    my $document = 0;
    if ($file =~ s/:(\d+)$//)
    {
        $document = $1;
    }

#     print "going to load document number $document in $file\n";

    my (@documents) = YAML::XS::LoadFile( $file );

    return $documents[$document];
}

sub checkAvailableOptions {
    my $format = shift;
    my $options = shift;

    my $checkoptions = {
        'csv'   => sub {
            my $options = shift;
            return  (defined($options->{'fields'}) and
                ref($options->{'fields'}) eq 'ARRAY' and
                defined($options->{'delimiter'}));
        },
        'json'   => sub {
            return  1;
        },
    };

    return $checkoptions->{$format}->($options);
}

sub parseEntry {
    my $format = shift;
    my $options = shift;
    my $entry = shift;

    my $parseEntry = {
        'csv'   => sub {
            my $options = shift;
            my $entry = shift;

            my $delimiter = $options->{'delimiter'};

            my $hash = {};
            (my @fields) = split(/$delimiter/, $entry);
            my $index = 0;
            foreach my $field (@{$options->{'fields'}})
            {
                $hash->{$field} = $fields[$index++];
            }

            return $hash;
        },
        'json'   => sub {
            my $options = shift;
            my $entry = shift;

            my $hash = JSON::XS::decode_json( $entry );
            return $hash;
        },
    };

    return $parseEntry->{$format}->($options, $entry);
}

1;
