
# Description

The program is originally designed to analyze, categorize and filter a stream of torrents, or rather the metadata of torrents. The current design, however, could easily allow for many different usages that deals with parsing of metadata-based streams.

The program is built around a number of _sources_, and a number of _actions_. Both the sources and the actions are defined around a number of existing _modules_, a different set for each.

The sources provides JSON-encoded data to the program, one entry at a time.

The core of the program loops through all entries in the `actions` field in an entry until it is empty.

Each action may use and/or change any fields in the current entry, including the list of actions to run.

# Howto

The fields necessary for basic usage in current configuration are:

* torrent  - the name of the torrent
* url - the url where the torrent file is located
* category - a category of the torrent, used to help with the torrent analysis

# Configuration file

The configuration is written in YAML.

The basic format of the file is:

    actions:
      myAction:
        module: myModule
        config:
          myConfig

      myAction2:
        module: myModule2
        config:
          myConfig2

    sources:
      mySource:
        module: myModule3
        config:
          myConfig3

      mySource2:
        module: myModule4
        config:
          myConfig4

    modulesettings:
      myModule2:
        myConfig6
      myModule3:
        myConfig7

Actions may have some additional fields:

    actions:
      myAction:
        module: myModule
        config:
          myConfig
        precond:
          values:
            ...
        postcond:
          values:
            ...

The `precond` and `postcond` are optional calls to the `setter` module, and can be used to make sure that certain fields exist before or after running an action, or to set fields before / clean up after the main module is run.

## Initializer

If no actions are specified initially, the action `initializer` is run, which can be used to add new actions to the pipeline.

An example of an `initializer` action is:

    actions:
      initializer:
        module: setter
        config:
          values:
          - set:
              actions:
              - mapper
              - analyzer
              - filterer

# Existing modules for sources

**subject to change**

## rss

      myRssSource:
        module: rss
        config:
          skipexisting: Yes
          interval: 30
          url: 'http://url.to/rss.xml'
          setter:
            values:
            - set:
                torrent: '{{rss-title}}'
                url: '{{rss-link}}'
                category: '{{rss-category}}'

## irc

      myIrcSource:
        module: irc
        config:
          ircsettings:
            server: my.irc.hostname
            port: 6667
            name: username
            nick: username
            alt_nicks: []
            username: username
            channels:
            - '#channel'
            quit_message: Goodbye
          setter:
            values:
            - irc-who: 'AnnounceBot'
              irc-body:
                regex: "New Torrent Announcement: <(.+)>  Name: '(.+)' uploaded by '(.*)' -  (.*)"
              set:
                category: '{{0}}'
                torrent: '{{1}}'
                uploader: '{{2}}'
                url: '{{3}}'

## pipe

      myPipeSource:
        module: pipe
        config:
          pipe: csvinput.fifo
          format: csv
          options:
            divider: ','
            fields:
            - torrent
            - url
            - category

## command_line

      myCommandlineSource:
        module: command_line
        config:
          format: json

# Existing modules for actions

## setter
This module sets fields based on conditions. It is very flexible, and can be used for many different tasks.

    actions:
      myAction:
        module: setter
        config:
          values:             # Setter-specific field of filters
          - field: value      # All field: value must match for the filter to match
            field2:           # Field2 must contain all values specified
              all:            # A qualifier, ex: not, any, all, max, min, regex
              - value1        #
              - value2
          - field:            # Another filter
            - value           # If no qualifier is specified, "any" is assumed
            - value2
            isset:            # Special field that matches if fields are set
            - field3
          - isset:
              not:            # isset may be used with qualifiers
              - field2
              - field3
            set:              # Special field, sets fields if the filter matched
              field1: value1
              field2: value2
            add:              # Special field, adds values to fields if the filter matched
              field1: value1
            inject:           # Special field, similar to add, but inserts values in beginning of field array
              field1: value1
            unset:            # Special field, deletes fields if the filter matched
            - field3
            - field4
          options:            # Optional setter-specific field
            run_all: Yes      # If run_all is set, all filters are tested,
                              # even if a previous filter matches.

The `set` command has some more special features

    set:
      field1: 'value1{value2{field2}value3}value4'
If `field2` is set, and has the value `value5`, the final value of `field1` will be `value1value2value5value3value4`.
If `field2` is not set, the final value of field1 will be `value1value4`.

    set:
      field1: '{{field2:n}}'
If the entry inside the brackets has a suffix `:n`, where `n` is a number, the field is padded to that length.

    set:
      field1: '<value1<regex1><value2>>'
All occurrences of `regex1` within `value1` will be replaced with `value2`.

These can be used to create advanced custom fields like a path:

    set:
      path: '<{{label}}{/{title}}{/S{season:2}}<\s+><_>>'

This creates paths such as `TV/The_Walking_Dead/S02` or `Anime/Naruto_Shippuuden` (anime usually don't have seasons).

Regular expressions can be run against fields. 

    field1:
      regex-any: '^(.*) - (\d+)$'

Regex captures can then be used in `set` commands, by using the matching number (starting from 0) as field name in `{{field}}`:

    set:
      field2: '{{0}}'
      field3: '{{1}}'

The `regex` qualifier is an alias for `regex-any`.

## download-ruTorrent

    download-ruTorrent:
      module: download-ruTorrent
      config:
        basepath: '/home/USER/torrents'
        rutorrent-url: 'http://USER:PASSWORD@HOST/rutorrent'
        stopped: 1
        label-field: longlabel

## create-rss

    make-rss-of-downloaded:
      module: create-rss
      config:
        file: '/path/to/rss.xml'
        feedtitle: Downloaded torrents
        feedurl: http://url.to/myfeed
        feeddescription: These torrents has been downloaded with tr;dl
        itemtitle: '{{torrent}}'
        itemurl: '{{url}}'
        itemdescription: '{{title}}: {{longlabel}}'
        itemlimit: 50
